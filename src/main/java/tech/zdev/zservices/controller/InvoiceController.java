package tech.zdev.zservices.controller;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tech.zdev.zservices.bean.AddServiceRQ;
import tech.zdev.zservices.bean.AddServiceRS;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/invoices")
public class InvoiceController {


    @GetMapping(value = "/new")
    public String goInterface(@RequestParam(value = "invoiceNumbers", required = false) String invoiceNumbers,
                              @RequestParam(value = "iata", required = false) String iata,
                              @RequestParam(value = "tktAgt", required = false) String tktAgt,
                              @RequestParam(value = "bkAgt", required = false) String bkAgt,
                              @RequestParam(value = "sellAgt", required = false) String sellAgt,
                              @RequestParam(value = "branch", required = false) String branch,
                              @RequestParam(value = "username", required = false) String username,
                              @RequestParam(value = "accountid", required = false) String accountid,
                              @RequestParam(value = "process", required = false) String process,
                              @RequestParam(value = "mailAgt", required = false) String mailAgt,
                              @RequestParam(value = "userId", required = false) String userId,
                              @RequestParam(value = "country", required = false) String country,
                              @RequestParam(value = "app", required = false) String app, Model model,
                              HttpServletRequest request) {
        HttpSession session = request.getSession();

        if (app != null && app.equalsIgnoreCase("admin")) {

            if (mailAgt != null && !mailAgt.isEmpty()) {
                session.setAttribute("mailAgt", mailAgt);
            }else{
                mailAgt = (String) session.getAttribute("mailAgt");
            }
            if (process != null && !process.isEmpty()) {
                session.setAttribute("process", process);
            }else{
                process = (String) session.getAttribute("process");
            }
            if (accountid != null && !accountid.isEmpty()) {
                session.setAttribute("accountid", accountid);
            }else{
                accountid = (String) session.getAttribute("accountid");
            }
            if (username != null && !username.isEmpty()) {
                session.setAttribute("username", username);
            }else{
                username = (String) session.getAttribute("username");
            }
            if (bkAgt != null && !bkAgt.isEmpty()) {
                session.setAttribute("bkAgt", bkAgt);
            }else{
                bkAgt = (String) session.getAttribute("bkAgt");
            }
            if (iata != null && !iata.isEmpty()) {
                session.setAttribute("iata", iata);
            }else{
                iata = (String) session.getAttribute("iata");
            }
            if (tktAgt != null && !tktAgt.isEmpty()) {
                session.setAttribute("tktAgt", tktAgt);
            }else{
                tktAgt = (String) session.getAttribute("tktAgt");
            }
            if (branch != null && !branch.isEmpty()) {
                session.setAttribute("branch", branch);
            }else{
                branch = (String) session.getAttribute("branch");
            }
            if(country != null && !country.isEmpty()){
                session.setAttribute("country", country);
            }else{
                country = (String) session.getAttribute("country");
            }

            model.addAttribute("addServiceRQ", new AddServiceRQ());
            model.addAttribute("iata", iata);
            model.addAttribute("tktAgt", tktAgt);
            model.addAttribute("bkAgt", bkAgt);
            model.addAttribute("sellAgt", sellAgt);
            model.addAttribute("branch", branch);
            model.addAttribute("process", process);
            model.addAttribute("accountid", session.getAttribute("accountid"));
            model.addAttribute("username", session.getAttribute("username"));
            model.addAttribute("mailAgt", session.getAttribute("mailAgt"));
            model.addAttribute("userId", userId);
//            model.addAttribute("offices", SisventasUtil.officesList());
            model.addAttribute("country",country);
            if (invoiceNumbers != null) {
                model.addAttribute("invoiceNumber", invoiceNumbers.split("-")[0]);
            }

        } else {
            return "redirect: redirect";
        }

        return "new";
    }

    @GetMapping(value = "/redirect")
    public void method(HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Location", "https://admin.costamar.com/");
        httpServletResponse.setStatus(302);
    }

    @PostMapping(value = "/new")
    public String generateInvoice(
            @RequestParam(value = "routeAtt", required = false) MultipartFile routeAtt,
            @RequestParam(value = "routeAtt1", required = false) MultipartFile routeAtt1,
            @RequestParam(value = "routeAtt2", required = false) MultipartFile routeAtt2,
            @RequestParam(value = "routeAtt3", required = false) MultipartFile routeAtt3,
            @RequestParam(value = "routeAtt4", required = false) MultipartFile routeAtt4,
            @RequestParam(value = "mail", required = false) String mail1,
            @RequestParam(value = "mail1", required = false) String mail2,
            @RequestParam(value = "mail2", required = false) String mail3,
            AddServiceRQ addServiceRQ, Model model) throws MessagingException {

        List<String> lista = new ArrayList<>();
        lista.add("liquidaciones@costamar.com.pe");
        lista.add("liquidaciones@costamarcorp.com");

        List<String> listaCC = new ArrayList<>();
        listaCC.add(addServiceRQ.getMailAgt());
        if (mail1 != null && !mail1.equals("")) {
            listaCC.add(mail1);
        }if (mail2 != null && !mail2.equals("")) {
            listaCC.add(mail2);
        }if (mail3 != null && !mail3.equals("")) {
            listaCC.add(mail3);
        }

        List<MultipartFile> listaMultipart = new ArrayList<>();
        if (routeAtt != null && !routeAtt.isEmpty()) {
            listaMultipart.add(routeAtt);
        }if (routeAtt1 != null && !routeAtt1.isEmpty()) {
            listaMultipart.add(routeAtt1);
        }if (routeAtt2 != null && !routeAtt2.isEmpty()) {
            listaMultipart.add(routeAtt2);
        }if (routeAtt3 != null && !routeAtt3.isEmpty()) {
            listaMultipart.add(routeAtt3);
        }if (routeAtt4 != null && !routeAtt4.isEmpty()) {
            listaMultipart.add(routeAtt4);
        }

        Gson gson = new Gson();
        String json = gson.toJson(addServiceRQ);

        model.addAttribute("addService", false);
        model.addAttribute("addServiceRS", new AddServiceRS());

        return "invoice";
    }
}
