package tech.zdev.zservices.bean;

public class PaymentInfo {

    private String type; //Plastic or Cash
    private CreditCard creditCard;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}
