package tech.zdev.zservices.bean;

public class NoteInfo {

	private String detailCollectionNote;
	private double totalCobrado;
	private boolean showTotal;	
	
	public boolean isShowTotal() {
		return showTotal;
	}
	public void setShowTotal(boolean showTotal) {
		this.showTotal = showTotal;
	}
	public String getDetailCollectionNote() {
		return detailCollectionNote;
	}
	public void setDetailCollectionNote(String detailCollectionNote) {
		this.detailCollectionNote = detailCollectionNote;
	}
	public double getTotalCobrado() {
		return totalCobrado;
	}
	public void setTotalCobrado(double totalCobrado) {
		this.totalCobrado = totalCobrado;
	}

	
	 
}
