package tech.zdev.zservices.bean;

public class Comment {

	private int id;
	private int type;
	private String content;
	private int payID;
	private String name;
	private Boolean showInv;


	public Boolean getShowInv() {
		return showInv;
	}

	public void setShowInv(Boolean showInv) {
		this.showInv = showInv;
	}

	public String getName() {
		return name;
	}

	public void setName(String comentName) {
		this.name = comentName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getPayID() {
		return payID;
	}

	public void setPayID(int payID) {
		this.payID = payID;
	}

}
