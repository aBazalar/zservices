package tech.zdev.zservices.bean;

import java.util.ArrayList;
import java.util.List;

public class ItemService {
	private String type;//ticketnumber 
	private String confirmationCode;
	private String invoiceNumber; 
	private String paperInvoiceNumber;
	private String startDate; //deperdate
	private String endDate; 	
	private String origin;
	private String destination;
	private String providerId;//SFEE 
	private String pnrAssociated;
	private String firstNamePassenger;
	private String lastNamePassenger;
	private String detail;
	private double payId;
	//Amounts
	private double baseAmount; //base fare
	private double commPercent; //commPercent
	private double commissionAmount; //commissionAmount
	private double paymentToProvider;
	private double igv;
	private double tax1;
	private double tax2;
	private double tax3;
	private double totalAmount;

	private String currencyType;
	private boolean domestic;
	private String resSystem;
	private String domInt;
	private String traveler;
	private List<Segment> segments;
	private List<Comment> comments;
	private String itinerary;

	public double getPaymentToProvider() {
		return paymentToProvider;
	}

	public void setPaymentToProvider(double paymentToProvider) {
		this.paymentToProvider = paymentToProvider;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public List<Comment> getComments() {
		if(comments == null){
			comments = new ArrayList<>();
		}
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getItinerary() {
		return itinerary;
	}

	public void setItinerary(String itinerary) {
		this.itinerary = itinerary;
	}

	public String getTraveler() {
		return traveler;
	}

	public void setTraveler(String traveler) {
		this.traveler = traveler;
	}

	public List<Segment> getSegments() {
		return segments;
	}

	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}

	public double getCommPercent() {
		return commPercent;
	}

	public void setCommPercent(double commPercent) {
		this.commPercent = commPercent;
	}

	public double getTax1() {
		return tax1;
	}

	public void setTax1(double tax1) {
		this.tax1 = tax1;
	}

	public double getTax2() {
		return tax2;
	}

	public void setTax2(double tax2) {
		this.tax2 = tax2;
	}

	public double getTax3() {
		return tax3;
	}

	public void setTax3(double tax3) {
		this.tax3 = tax3;
	}

	public String getResSystem() {
		return resSystem;
	}

	public void setResSystem(String resSystem) {
		this.resSystem = resSystem;
	}

	public String getDomInt() {
		return domInt;
	}

	public void setDomInt(String domInt) {
		this.domInt = domInt;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public double getIgv() {
		return igv;
	}

	public void setIgv(double igv) {
		this.igv = igv;
	}

	public double getPayId() {
		return payId;
	}

	public void setPayId(double payId) {
		this.payId = payId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPaperInvoiceNumber() {
		return paperInvoiceNumber;
	}

	public void setPaperInvoiceNumber(String paperInvoiceNumber) {
		this.paperInvoiceNumber = paperInvoiceNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getPnrAssociated() {
		return pnrAssociated;
	}

	public void setPnrAssociated(String pnrAssociated) {
		this.pnrAssociated = pnrAssociated;
	}

	public String getFirstNamePassenger() {
		return firstNamePassenger;
	}

	public void setFirstNamePassenger(String firstNamePassenger) {
		this.firstNamePassenger = firstNamePassenger;
	}

	public String getLastNamePassenger() {
		return lastNamePassenger;
	}

	public void setLastNamePassenger(String lastNamePassenger) {
		this.lastNamePassenger = lastNamePassenger;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public double getCommissionAmount() {
		return commissionAmount;
	}

	public void setCommissionAmount(double commissionAmount) {
		this.commissionAmount = commissionAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public boolean isDomestic() {
		return domestic;
	}

	public void setDomestic(boolean domestic) {
		this.domestic = domestic;
	}
}
