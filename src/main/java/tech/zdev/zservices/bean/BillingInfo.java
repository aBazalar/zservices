package tech.zdev.zservices.bean;

public class BillingInfo {
	private String officeId;
	private String documentId;
	private String seriesId;
	private String username;
	private String firstname;
	private String address;
	private String saleId;
	private String refId;
	private String imprime;
	private String observacion;
	private String clienteId;
	private String addresSisventas;
	private String firstnameSisventas;
	private String ruc;
	
	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getAddresSisventas() {
		return addresSisventas;
	}

	public void setAddresSisventas(String addresSisventas) {
		this.addresSisventas = addresSisventas;
	}

	public String getFirstnameSisventas() {
		return firstnameSisventas;
	}

	public void setFirstnameSisventas(String firstnameSisventas) {
		this.firstnameSisventas = firstnameSisventas;
	}

	public String getClienteId() {
		return clienteId;
	}

	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getImprime() {
		return imprime;
	}

	public void setImprime(String imprime) {
		this.imprime = imprime;
	}

	public String getOfficeId() {
		return officeId;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSaleId() {
		return saleId;
	}

	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

}
