package tech.zdev.zservices.bean;

public class ItemServiceInserted extends ItemService {
	private boolean saved;
	private boolean inserted;

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public boolean isInserted() {
		return inserted;
	}

	public void setInserted(boolean inserted) {
		this.inserted = inserted;
	}
}
