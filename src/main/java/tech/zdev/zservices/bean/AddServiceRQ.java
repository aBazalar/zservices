package tech.zdev.zservices.bean;

import java.util.ArrayList;
import java.util.List;

public class AddServiceRQ {
	private String accountId;
	private String customerType;
	private String bookingAgentId;
	private String issuingAgentId;
	private String tktAgentId;
	private String officeCode;
	private PaymentInfo paymentInfo;
	private String firstNamePassenger;
	private String lastNamePassenger;
	private List<ItemService> itemServices;
	private boolean requiresBilling;
	private boolean billAll;
	private boolean mailValid;
	private BillingInfo billingInfo;
	private String fee;
	private String commissionAgy;
	private String detailInvoice;
	private List<Comment> itemComments;
	private String cantPaxes;
	private String merchant;
	private String personaAutoriza;
	private String accountName;
	private String process;
	private String mailAgt;
	private boolean showFee;
	private NoteInfo noteInfo;
	private String codePackage;
	private String country;
	private boolean exonerated;
	private double currencyType;
	private boolean currencyChange;
	private double totalInv;


	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public double getTotalInv() {
		return totalInv;
	}

	public void setTotalInv(double totalInv) {
		this.totalInv = totalInv;
	}

	public String getCommissionAgy() {
		return commissionAgy;
	}

	public void setCommissionAgy(String commissionAgy) {
		this.commissionAgy = commissionAgy;
	}

	public double getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(double currencyType) {
		this.currencyType = currencyType;
	}

	public boolean isCurrencyChange() {

		return currencyChange;
	}

	public void setCurrencyChange(boolean currencyChange) {
		this.currencyChange = currencyChange;
	}

	public boolean isExonerated() {
		return exonerated;
	}

	public void setExonerated(boolean exonerated) {
		this.exonerated = exonerated;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCodePackage() {
		return codePackage;
	}

	public void setCodePackage(String codePackage) {
		this.codePackage = codePackage;
	}

	public NoteInfo getNoteInfo() {
		return noteInfo;
	}

	public void setNoteInfo(NoteInfo noteInfo) {
		this.noteInfo = noteInfo;
	}

	public boolean isShowFee() {
		return showFee;
	}

	public void setShowFee(boolean showFee) {
		this.showFee = showFee;
	}

	public String getMailAgt() {
		return mailAgt;
	}

	public void setMailAgt(String mailAgt) {
		this.mailAgt = mailAgt;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getDetailInvoice() {
		return detailInvoice;
	}

	public void setDetailInvoice(String detailInvoice) {
		this.detailInvoice = detailInvoice;
	}

	public String getTktAgentId() {
		return tktAgentId;
	}

	public void setTktAgentId(String tktAgentId) {
		this.tktAgentId = tktAgentId;
	}
	
	public boolean isMailValid() {
		return mailValid;
	}

	public void setMailValid(boolean mailValid) {
		this.mailValid = mailValid;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getBookingAgentId() {
		return bookingAgentId;
	}

	public void setBookingAgentId(String bookingAgentId) {
		this.bookingAgentId = bookingAgentId;
	}

	public String getIssuingAgentId() {
		return issuingAgentId;
	}

	public void setIssuingAgentId(String issuingAgentId) {
		this.issuingAgentId = issuingAgentId;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getFirstNamePassenger() {
		return firstNamePassenger;
	}

	public void setFirstNamePassenger(String firstNamePassenger) {
		this.firstNamePassenger = firstNamePassenger;
	}

	public String getLastNamePassenger() {
		return lastNamePassenger;
	}

	public void setLastNamePassenger(String lastNamePassenger) {
		this.lastNamePassenger = lastNamePassenger;
	}

	public List<ItemService> getItemServices() {
		return itemServices;
	}

	public void setItemServices(List<ItemService> itemServices) {
		this.itemServices = itemServices;
	}

	public boolean isRequiresBilling() {
		return requiresBilling;
	}

	public void setRequiresBilling(boolean requiresBilling) {
		this.requiresBilling = requiresBilling;
	}

	public boolean isBillAll() {
		return billAll;
	}

	public void setBillAll(boolean billAll) {
		this.billAll = billAll;
	}

	public BillingInfo getBillingInfo() {
		return billingInfo;
	}

	public void setBillingInfo(BillingInfo billingInfo) {
		this.billingInfo = billingInfo;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getComments() {
		return detailInvoice;
	}

	public void setComments(String comments) {
		this.detailInvoice = comments;
	}

	public List<Comment> getItemComments() {
		if(itemComments == null){
			itemComments = new ArrayList<>();
		}
		return itemComments;
	}

	public void setItemComments(List<Comment> itemComments) {
		this.itemComments = itemComments;
	}

	public String getCantPaxes() {
		return cantPaxes;
	}

	public void setCantPaxes(String cantPaxes) {
		this.cantPaxes = cantPaxes;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getPersonaAutoriza() {
		return personaAutoriza;
	}

	public void setPersonaAutoriza(String personaAutoriza) {
		this.personaAutoriza = personaAutoriza;
	}

}
