package tech.zdev.zservices.bean;

import java.util.ArrayList;
import java.util.List;

public class AddServiceRS {
	private List<ItemServiceInserted> itemServiceInserteds;
	private BillingInfo billingInfo;

	public List<ItemServiceInserted> getItemServiceInserteds() {
		if(itemServiceInserteds == null){
			itemServiceInserteds = new ArrayList<>();
		}
		return itemServiceInserteds;
	}

	public void setItemServiceInserteds(List<ItemServiceInserted> itemServiceInserteds) {
		this.itemServiceInserteds = itemServiceInserteds;
	}

	public BillingInfo getBillingInfo() {
		return billingInfo;
	}

	public void setBillingInfo(BillingInfo billingInfo) {
		this.billingInfo = billingInfo;
	}

}
