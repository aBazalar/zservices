package tech.zdev.zservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZservicesApplication.class, args);
	}

}
