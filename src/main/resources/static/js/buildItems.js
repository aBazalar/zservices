$(document).ready(function () {
    var proccessCorp = $('input[name=process]').val().toLowerCase();

    $('#add-segment-btn').click(function(){
        if(validateFormSegment()){
            if (new Date($('#departDate').val()).getTime() > new Date($('#arrivalDate').val()).getTime()) {
                jAlert("Las fechas ingresadas son incorrectas: <br/><br/> " +
                    "Fecha de regreso no puede ser antes de Fecha Inicio. <br/>" +
                    "Periodo errado : Del " + $("#end-date").val() +" al " +$("#start-date").val()+".","Intervalo de fechas");
            }else{
                $("#table-segments tbody").append($('<tr>').append(buildSegment())
                    .append($('<td>').text($('#flightNumber').val()))
                    .append($('<td>').text($('#airlineSegment').val()))
                    .append($('<td>').text($('#departCity').val()))
                    .append($('<td>').text($('#arrivalCity').val()))
                    .append($('<td>').text($('#departDate').val()))
                    .append($('<td>').text($('#arrivalDate').val()))
                    .append($('<td>').text($('#classSegment').val()))
                    .append($('<td>').append($("#delete-row").html())));
                clearFormSegments();

            }
        }
        else {
            jAlert("Complete todos los campos en blanco", 'Campos Requeridos');
        }
        sortSegments();
    });
    function validateFormSegment(){
        var validation = true;
        $('#form-tkts-segments input').each(function(){
            if ($(this).val() == '') {
                $(this).css('border-color', '#f8ac59');
                validation = false;
            } else {
                $(this).css('border-color', '#d5d5d5');
            }
        });
        return validation;
    }
    function buildSegment(){
        return $("#tkt-segment").html()
            .replace("airline-value", $('#airlineSegment').val())
            .replace("arrivalCity-value", $('#arrivalCity').val())
            .replace("arrivalDate-value", $('#arrivalDate').val())
            .replace("classSegment-value", $('#classSegment').val())
            .replace("departCity-value", $('#departCity').val())
            .replace("departDate-value", $('#departDate').val())
            .replace("flight-value",$('#flightNumber').val());
    }
    function sortSegments(){
        $('form .airlineSegment').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .arrivalCity').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .arrivalDate').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .classSegment').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .departCity').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .departDate').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .flightNumber').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });

    }

    var numServices = 0;
    // Agregar servicio
    $('#add-service-btn').click(function() {
        if (validateForm()) {
            if (new Date($('#start-date').val()).getTime() > new Date($('#end-date').val()).getTime()) {
                jAlert("Las fechas ingresadas son incorrectas: <br/><br/> " +
                    "Fecha Fin no puede ser antes de Fecha Inicio. <br/>" +
                    "Periodo errado : Del " + $("#end-date").val() +" al " +$("#start-date").val()+".","Intervalo de fechas");
            }
            else if (validateSameConfirmationCode()){
                jAlert("El código de confimación está registrado, favor de cambiarlo","Código de Confirmación de los Servicios");
            } else {
                $("#table-services tbody").append($('<tr>')
                    .append(buildService())
                    .append(appendComments())
                    .append($('<td>').text($('#invoice-number').val()))
                    .append($('<td>').text($('#service-option :selected').text()))
                    .append($('<td>').text($('#provider').val()))
                    .append($('<td>').text($('#iata').val()))
                    .append($('<td>').text($('#total-amount').val()))
                    .append($('<td>').append($("#delete-row").html())));

                sortComments();
                sortServicesSegments();
                sortServicesComments();
                numServices++;
                clearFormServiceBox();
            }
        }
        else {
            jAlert("Complete todos los campos en blanco", 'Campos Requeridos');
        }
        sortServices();
        if ($('#service-option').val() == 'I') {
            if ($('#firstname-passenger').val() == "" && $('#lastname-passenger').val() == "") {
                $('#firstname-passenger').val($('#firstname-insured').val());
                $('#lastname-passenger').val($('#lastname-insured').val());
            }
        }
        $('#firstname-insured').val("");
        $('#lastname-insured').val("");
        calTotalCobrado();
    });

    function appendComments(){
        var comments = "";
        if(proccessCorp==("corp").toLowerCase()){
            $("#form-service-box .comment").each(function () {
                var content, id, name, showinv;
                if($(this).val() != "" && !$(this).is(":hidden")){
                    if($(this).attr('type') == 'checkbox'){
                        content = $(this).is(':checked');
                    }else{
                        content = $(this).val();
                    }
                    id= $(this).next().val();
                    name = $(this).next().next().val();
                    showinv =$(this).next().next().next().val();
                    comments += buildServiceComments(id,content,name,showinv);
                }
            });
        }

        return comments;
    }
    function validateForm() {
        var validation = true;
        $('#form-service-box input').not("#codePackage, #flightNumber, #check-in, #check-out,#totHotel ,#airlineSegment, #departCity, #arrivalCity, #departDate, #arrivalDate, #classSegment, #idAutogenerated, #idCalcularIGV ").each(function() {
            if ($(this).val() == '' && !($(this).is(':hidden'))) {
                $(this).css('border-color', '#f8ac59');
                validation = false;
            } else {
                $(this).css('border-color', '#d5d5d5');
            }
        });
        if($('#provider-id').val() == ''){
            validation = false;
        };
        if($('#service-option').val() == -1){
            $(this).css('border-color', '#f8ac59');
            validation = false;
        }
        return validation;
    }
    function validateSameConfirmationCode(){
        var validation = false;
        if($('.confirmation-code').length > 1){
            var codeConfirmationToAdd = $("#confirmation-code").val();
            $('.table-striped .confirmation-code').each(function() {
                if($(this).val() == codeConfirmationToAdd ){
                    validation = true;
                }
            });
        }
        return validation;
    }
    //BUILD ITEM SERVICE
    function buildService() {
        var igv = fixed($('#igv').val());
        var base=  fixed($('#base-amount').val());
        var tax1 = fixed($('#tax1').val());
        var tax2 = fixed($('#tax2').val());
        var tax3 = fixed($('#tax3').val());
        var com = fixed($('#commPercent').val());
        var total = fixed($("#total-amount").val());
        var paymetToProvider = fixed($("#paymentToProvider").val())

        return $("#item-service").html()
            .replace("type-value", $('#service-option :selected').val())
            .replace("confirmation-value", $('#confirmation-code').val())
            .replace("invoice-value", $('#invoice-number').val())
            .replace("start-value", $('#start-date').val())
            .replace("end-value", $('#end-date').val())
            .replace("destination-value", $('#iata').val())
            .replace("provider-value", $('#provider-id').val())
            .replace("pnr-value", $('#pnr').val())
            .replace("base-value",base)
            .replace("commission-value", 0)
            .replace("total-value", $("#total-amount").val())
            .replace("item-igv", igv)
            .replace("name-hotel",getDetailService())
            .replace("commPercent-value",com)
            .replace("tax1-value",tax1)
            .replace("tax2-value",tax2)
            .replace("tax3-value",tax3)
            .replace("pnr-value",$('#pnr').val())
            .replace("traveler-value",$('#apellido-traveler').val()+"/"+$('#nombre-traveler').val())
            .replace("resSystem-value",$('#resSystem').val())
            .replace("domInt-value",$('#domInt').val())
            .replace("paymentToProvider-value", paymetToProvider)
            .replace("currencyType-value", $("#currency-type-provider").val())
            .replace("itinerary-value",$('#itinerary').val())
    }
    function sortServices() {
        $('form .type').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .confirmation-code').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .invoice-number').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .start-date').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .end-date').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .destination').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .provider').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .pnr').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .base-amount').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .commission-amount').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .total-amount').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .first-name').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .last-name').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .igv').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .hotel').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .comm-percent').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .tax1').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .tax2').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .tax3').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .traveler').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .gds').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .domInt').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .itinerary').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $("form .paymentToProvider").each(function (index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index))
        });
        $("form .currencyType").each(function (index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index))
        });
    }
    //SORT SEGMENTS
    function sortServicesSegments(){
        $('form .airlineSegment').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("airlineSegment");
        });
        $('form .arrivalCity').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("arrivalCity");
        });
        $('form .arrivalDate').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("arrivalDate");
        });
        $('form .classSegment').each(function(
            index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("classSegment");
        });
        $('form .departCity').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("departCity");
        });
        $('form .departDate').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("departDate");
        });
        $('form .flightNumber').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("flightNumber");
        });

    }
    //BUILD COMMENTS
    function buildServiceComments(id, content , name , showinv) {
        return $('#item-comments').html()
            .replace("commentID-value",id)
            .replace("content-value",content)
            .replace("commentName-value",name)
            .replace("showInv-value",showinv);

    }
    function sortServicesComments(){
        $('form .itemCommentID').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("itemCommentID");
        });
        $('form .itemContent').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("itemContent");
        });
        $('form .itemCommentName').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("itemCommentName");
        });
        $('form .itemShowInv').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/numServices/g, numServices));
            $(this).removeClass("itemShowInv");
        });
    }
    function  sortComments() {
        $('form .itemCommentID').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .itemContent').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .itemCommentName').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
        $('form .itemShowInv').each(function(index) {
            $(this).attr("name", $(this).attr("name").replace(/index/g, index));
        });
    }

    //DELETE ROW
    $(document).on("click", '.delete-service-btn', function(index) {
        console.log(index);
        $(this).closest("tr").remove();
        sortServices();
        numServices = $('.table-striped .confirmation-code').size()
        if(numServices == -1){numServices = 0};
    });
});

function getDetailService(){
    var detail = "";
    switch ($('#service-option').val()){
        case "H" : detail = $("#hotelName").val(); break;
        case "I" : detail = " Tarjeta de asistencia"; break;
        case "B" : detail = "Transporte terrestre"; break;
        case "C" : detail = "Alquiler de auto"; break;
        case "R" : detail = "Reserva de tren"; break;
        case "S" : detail = "Reserva de crucero";break;
        case "T" : detail = "Tour en" + $("#iata").val(); break;
        case "O" : detail = $("#other-type").val(); break;

    }
    return detail;
}

