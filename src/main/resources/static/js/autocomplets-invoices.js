$(document).ready(function(){
	
	//	Autocomplet Hoteles
	$('#hotelName').autocomplete({
		source : function(request, response) {				
			var regex = new RegExp(request.term, 'i');
			$.ajax({
				url : "json/hotels.json?v=1.0.1.7",
				dataType : "json",
				success : function(data) {
					if (data.hotels != null) {
						response($.map(data.hotels, function(e) {
							if (regex.test(e.name)) {
								return {
									id : e.code,
									label : e.name,
									value : e.name
								};
							}
						}));
					}
				}
			});
		},
		minLength : 3,
		select : function(event, ui) {
			$(this).prev().val(ui.item.id);
		},
		open : function() {
			$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close : function() {
			$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});
	//Autocomplet Aerolineas 
	$('#airlineSegment').autocomplete({
		source : function(request, response) {				
			var regex = new RegExp(request.term, 'i');
			$.ajax({
				url : "json/airlines.json?v=1.0.1.6",
				dataType : "json",
				success : function(data) {
					if (data.RECORDS != null) {
						response($.map(data.RECORDS, function(e) {
							if (regex.test(e.name)) {
								return {
									label : e.name,
									value : e.char_code
								};
							}
						}));
					}
				}
			});
		},
		minLength : 3,
		select : function(event, ui) {
			$(this).prev().val(ui.item.id);
		},
		open : function() {
			$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close : function() {
			$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});
	//Autocomplet Airports 
	$('#departCity , #arrivalCity, #iata').autocomplete({
		source : function(e, t) {

			$.ajax({
				type : 'GET',
				url : 'https://api.zdev.tech/z-api-0.1/rest/Airports/search',
				dataType : 'jsonp',
				data : {
					language : 'es',
					query : e.term
				},
				success : function(e) {
					if (e.airports != null) {
						t($.map(e.airports.items, function(e) {
							return {
								id : e.code,
								label : e.place,
								value : e.code
							};
						}));
					}
				}
			});
		},
		minLength : 2,
		select : function(e, t) {
			$(this).removeClass('ui-autocomplete-loading');
			$(this).prev().prev().val(t.item.id);
		},
		open : function() {
			$(this).removeClass('ui-corner-all').addClass('ui-corner-top');
		},
		close : function() {
			$(this).removeClass('ui-corner-top').addClass('ui-corner-all');
		}
	});	
	//Autocomplete Provider
	$('#provider').autocomplete({
		source : function(request, response) {
				var regex = new RegExp(request.term, 'i');
				$.ajax({
					type : 'POST',
					url : "provider/get",
					dataType : "json",
					data : { searchProvider : $('#provider').val(), country: $('#country').val()},
					success : function(data) {
						if (data != null) {
							response($.map(data, function(e) {
								if (regex.test(e.name) || regex.test(e.code) ) {
									return {
										id : e.code,
										label : e.code + " - " +e.name,
										value : e.name
									};
								}
							}));
						}
					}
				});
			},
		minLength : 3,
		select : function(event, ui) {
			$('#provider-id').val(ui.item.id);
		},
		open : function() {
			$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close : function() {
			$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});
	//AUTOCOMPLETS AGENTES CORP
	$('#bokkingName').autocomplete({
			source : function(request, response) {				
				var regex = new RegExp(request.term, 'i');
				$.ajax({
					url : "json/agentesCorp.json?v=1.0.1.7",
					dataType : "json",
					success : function(data) {
						if (data.hotels != null) {
							response($.map(data.hotels, function(e) {
								if (regex.test(e.name)) {
									return {
										id : e.code,
										label : e.name,
										value : e.name
									};
								}
							}));
						}
					}
				});
			},
			minLength : 3,
			select : function(event, ui) {
				$(this).prev().val(ui.item.id);
			},
			open : function() {
				$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
			},
			close : function() {
				$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
			}
		});

	//AUTOCOMPLET TICKETS KIU
    // $('#confirmation-code').blur(function () {
    	// if ($("#service-option").val() == 'A') {
     //        $('.please-wait-tickets').removeClass("hidden");
     //        var url = "http://34.220.231.53/kiubox-service/getTicketInfo/" + $('#confirmation-code').val();
     //        console.log(url);
     //        $.ajax({
     //            type: 'GET',
     //            dataType: "json",
     //            contentType: "application/json",
     //            url: url,
     //            success: function (data) {
     //                if (data.kiutktDisplay != null && data.kiutktDisplay != 'undefined') {
     //                    $("#table-segments tbody").empty();
	// 					var ticketDisplay = data.kiutktDisplay;
     //                    console.log(ticketDisplay);
    //
     //                    //Amounts
	// 					$("#base-amount").val(ticketDisplay.baseFare.amount);
	// 					$("#commPercent").val(ticketDisplay.commission.amount);
    //
	// 					var residualTax = 0;
	// 					for(var i = 0 ; i < ticketDisplay.taxes.tax.length ; i++) {
	// 						if(ticketDisplay.taxes.tax[i].taxCode == "PE") {
     //                            $("#tax1").val(ticketDisplay.taxes.tax[i].amount);
     //                            continue;
	// 						} else if (ticketDisplay.taxes.tax[i].taxCode == "HW") {
	// 							$("#tax3").val(ticketDisplay.taxes.tax[i].amount);
	// 							continue;
	// 						} else {
     //                            residualTax += ticketDisplay.taxes.tax[i].amount;
	// 						}
	// 					}
     //                    $("#tax2").val(residualTax);
    //
	// 					$("#total-amount").val(ticketDisplay.totalFare.amount);
	// 					var fullTicketNumber = $("#confirmation-code").val();
	// 					var provider = fullTicketNumber.substring(0, 3);
	// 					var confirmationCode = fullTicketNumber.substring(3, fullTicketNumber.length);
	// 					$("#provider-id").val(provider);
	// 					$("#provider").val(provider);
	// 					$("#confirmation-code").val(confirmationCode);
    //
    //
	// 					$("#nombre-traveler").val(ticketDisplay.passengerName.givenName);
	// 					$("#firstname-passenger").val(ticketDisplay.passengerName.givenName);
	// 					$("#apellido-traveler").val(ticketDisplay.passengerName.surname);
	// 					$("#lastname-passenger").val(ticketDisplay.passengerName.surname);
	// 					$("#pnr").val(ticketDisplay.bookingReferenceID.id);
     //                    $("#resSystem").val("KI");
    //
     //                    //Segments
     //                    for(var j=0; j < ticketDisplay.flightReference.length ; j++) {
	// 						var segment =  ticketDisplay.flightReference[j].flightSegment;
	// 						if (j == 0) {
	// 							$("#start-date").val(segment.departureDateTime);
	// 						}
	// 						if (j == ticketDisplay.flightReference.length - 1) {
     //                            $("#end-date").val(segment.departureDateTime);
	// 						}
	// 						console.log(segment);
     //                        $("#table-segments tbody").append($('<tr>').append(buildSegment(segment))
     //                            .append($('<td>').text(segment.flightNumber))
     //                            .append($('<td>').text(segment.marketingAirline))
     //                            .append($('<td>').text(segment.departureAirport))
     //                            .append($('<td>').text(segment.arrivalAirport))
     //                            .append($('<td>').text(segment.departureDateTime))
     //                            .append($('<td>').text(segment.departureDateTime))
     //                            .append($('<td>').text(segment.resBookDesigCode))
     //                            .append($('<td>').append($("#delete-row").html())));
     //                        sortSegments();
     //                    }
	// 					$("#form-tkts-segments").addClass("hidden");
     //                    $('.please-wait-tickets').addClass("hidden");
     //                    $("#iata").css('border-color', '#f8ac59');
     //                    $("#alert-kiu-ticket").removeClass("hidden");
     //                    // $("#start-date").val();
     //                    // $("#end-date").val();
     //                }
     //                else {
     //                    $('.please-wait-tickets').addClass("hidden");
     //                    alert("No encontramos tu ticket, por favor ingresa toda la informacion requerida.")
	// 				}
     //            },
	// 			error : function (err) {
     //                $('.please-wait-tickets').addClass("hidden");
	// 				alert("No encontramos tu ticket, por favor ingresa toda la informacion requerida.")
     //            }
     //        });
     //    }
    // });

});